// 0,0 0,1 0,2
// 1,0 1,1 1,2
// 2,0 2,1 2,2

#include <iostream>

#include "matrix.h"

using namespace std;

void print(Matrix<int> tmp){
    cout <<endl;
    for (int i=0; i<5; ++i){
        for (int j=0; j<5; ++j){
            cout << tmp(i,j) << "\t";
        }
        cout <<endl;
    }
}

int main(int argc, char *argv[]) {
    Matrix<int> a(5, 5);

    int counter=1;
    for (int i=0; i<5; ++i){
        for (int j=0; j<5; ++j){
            a.set(i,j, counter);
        }
    }

    Matrix<int> b(5, 5);
    counter=5;
    for (int i=0; i<5; ++i){
        for (int j=0; j<5; j++){
            b.set(i,j, counter);
        }
    }

    cout << "\nMatrix a";
    print(a);

    cout << "\nMatrix b";
    print(b);

    cout << "\nMatrix a*3";
    auto c = a*3;
    print(c);



    // auto c = a-b;

    // for (int i=0; i<5; ++i){
    //     for (int j=0; j<5; ++j){
    //         cout << (a)(i,j) << "\t";
    //     }
    //     cout <<endl;
    // }

    return EXIT_SUCCESS;
}
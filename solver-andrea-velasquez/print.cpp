#include <iostream>
#include <cctype>
using namespace std;

#define COUNT 10  //BORRAR

//------------------ PRINT BEGIN
void print2DUtil(Node *root, int space)
{
    // Base case
    if (root == NULL)
        return;

    // Increase distance between levels
    space += COUNT;

    // Process right child first
    print2DUtil(root->right, space);

    // Print current node after space
    // count
    cout<< "\n";
    for (int i = COUNT; i < space; i++)
        cout << " ";
    //if (isdigit(*root->data)) cout << "#";
    cout << root->data <<endl; //---------------------------------------

    // Process left child
    print2DUtil(root->left, space);
}

// Wrapper over print2DUtil()
void print2D(Node* root)
{
   // Pass initial space count as 0
   print2DUtil(root, 0);
}

//------------------ PRINT END

#ifndef PARSER_H
#define PARSER_H

#include <cctype> //isdigit ()
#include <map>

bool isOperator(char x);

struct Parser {
  map<char, int> var;
  vector<string> tokenList;

  vector<string> parse(char* expression){
    vector<string> tokenList;
    map<char, float> var;
    string number = "";
    for (char* c=expression; c!= expression + strlen(expression); c++){
      if (isOperator(*c)) { tokenList.push_back(string(1, *c)); } // c es op
      else if (isdigit(*c)) { // c es digito
        number+=*c;
        if (!isdigit(*(c+1))) { //si el siguiente no es un digito
          tokenList.push_back(number);
          number = "";
        }
      }
      else if (var.count(*c)) tokenList.push_back(to_string(var[*c])); //known variable
      else {
        cout << "Enter value of " << *c << ": ";
        cin >> var[*c];
        tokenList.push_back(to_string(var[*c]));
      }
    }
    print2D(root);
    return tokenList;
  }


};

#endif




bool parse(char* expression){
  vector<string> tokenList;
  map<char, int> var;
  string number = "";
  for (char* c=expression; c!= expression + strlen(expression); c++){
    if (isOperator(*c)) { tokenList.push_back(string(1, *c)); } // c es op
    else if (isdigit(*c)) { // c es digito
      number+=*c;
      if (!isdigit(*(c+1)) && *(c+1)!='.') { //si el siguiente no es un digito
        tokenList.push_back(number);
        number = "";
      }
    }
    else{
      //variable can only be between operators
      if ( !isOperator(*(c-1)) || !isOperator(*(c+1)) ) return false;
      //known variable
      if (var.count(*c)) tokenList.push_back(to_string(var[*c]));
      else {
        cout << "Enter value of " << *c << ": ";
        cin >> var[*c];
        tokenList.push_back(to_string(var[*c]));
      }
    }
  }
  print2D(root);
  return true;
};


//----------------DEBUG
int parse(char* expression){
  //this->include
  map<char, float> var;
  string number = "";
  int counter = 0;
  for (char* c=expression; c!= expression + strlen(expression); c++){
    // c is operand
    if (isOperator(*c)) {cout << "\nToken: " << (string(1, *c)); continue;}
    // c is digit or point (part of a number)
    else if (isdigit(*c) || *c=='.') {
      if (*c=='.' && !isdigit(*(c+1))) {cout<<"1 "; return counter;}//incomplete decimal
      number+=*c;

      // if next character isn't part of a number, end number
      if (!(isdigit(*(c+1)) || *(c+1)=='.')) {
        cout << "\nToken: " <<(number);
        number = "";
        continue;
      }
    }
    else{
      // variable must be between operators
      //if ( !isOperator(*(c-1)) || !isOperator(*(c+1)) ) {cout<<"2 "; return counter;}
      //known variable
      if (var.count(*c)) {cout << "\nToken: " <<(to_string(var[*c])); continue;}
      else{
        cout << "Enter value of " << *c << ": ";
        cin >> var[*c];
        cout << "\nToken: " <<(to_string(var[*c]));
        continue;
      }
    }
    ++counter;
  }
print2D(root);
//cout << "\nAnswer: " << evaluate(root);
return -1;
};

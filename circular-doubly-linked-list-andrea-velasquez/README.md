# Double Linked-List

Original Github Link: [https://github.com/UTEC-PE/circular-doubly-linked-list-andrea-velasquez](https://github.com/UTEC-PE/circular-doubly-linked-list-andrea-velasquez)

Implementation of the double linked list and its iterator.
All the functions in node.h, list.h and iterator.h must be implemented.

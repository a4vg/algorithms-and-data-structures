# Linked List Task

Original Github Link: [https://github.com/UTEC-PE/linked-list-andrea-velasquez](https://github.com/UTEC-PE/linked-list-andrea-velasquez)

Implement all the functions in the .h

## Progress
### list.h
- [x] List()
- [x] ~List()
- [x] T front()
- [x] T back()
- [x] void push_front(T value)
- [x] void push_back(T value)
- [x] void pop_front()
- [x] void pop_back()
- [x] T get(int position)
- [x] void concat(List<T> &other)
- [x] bool empty()
- [x] int size()
- [x] void print()
- [x] void print_reverse()
- [x] void clear()

### node.h
- [x] void killSelf()

### iterator.h
- [x] Iterator()
- [x] Iterator(Node<T>* node)
- [x] operator =
- [x] operator !=
- [x] operator ++
- [x] operator *

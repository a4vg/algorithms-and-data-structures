# Algorithms and Data Structures
UTEC-CS2100 assignments and project.

This is a collection of repositories that were created with Github Classroom. Each directory contains a link to the original repositories in Github. The code may include tiny differences with the original repo due to uncommited changes during the assignment.
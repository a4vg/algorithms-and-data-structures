# Matrix Multiplication

Original Github Link: [https://github.com/UTEC-PE/matrix-multiplication-andrea-velasquez](https://github.com/UTEC-PE/matrix-multiplication-andrea-velasquez)

## Instructions
* Define row size and column size of the matrices with M N P
```
Matrix    [row][column]
Matrix 1  [M][N]
Matrix 2  [N][P]
```
* Matrices will be filled with consecutive numbers

* To multiply matrices with other numbers, modify matrix1 and matrix2 declaration
``` c++
auto matrix1 = new int[M][N] {{23,54},{2,65}}
```
